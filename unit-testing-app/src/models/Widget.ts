export interface Widget {
  id: number;
  name: string;
  description: string;
  size: string
  color: string;
  price: number;
  quantity: number;
}