import * as React from 'react';
import * as TestUtils from 'react-dom/test-utils';
import { mount, configure, ReactWrapper } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';

import { HelloWorld } from './hello-world';

configure({ adapter: new Adapter() });

describe('<HelloWorld /> Test Utils Mock DOM', () => {

  let component: React.Component;
  let componentDOMNode: Element;

  beforeEach(() => {
    component = TestUtils.renderIntoDocument(<HelloWorld />) as React.Component;
    componentDOMNode = TestUtils.findRenderedDOMComponentWithTag(component, 'h1');
  });

  test('<HelloWorld /> renders', () => {
    expect(componentDOMNode.textContent).toBe('Hello World!');
  });

});

describe('<HelloWorld /> Enzyme Mock DOM', () => {

  let component;
  let componentDOMNode: ReactWrapper;

  beforeEach(() => {
    component = mount(<HelloWorld />);
    componentDOMNode = component.find('h1');
  });

  test('<HelloWorld /> renders', () => {
    expect(componentDOMNode.text()).toBe('Hello World!');
  });

});