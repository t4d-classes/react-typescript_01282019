import * as React from 'react';
import * as TestUtils from 'react-dom/test-utils';
import { mount, configure, ReactWrapper } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';

import { ColorForm, Props, State } from './color-form';

configure({ adapter: new Adapter() });

jest.unmock('./color-form');

describe('<ColorForm /> Test Utils Mock DOM', () => {

  const colorValue = 'purple';
  const eventHandlers = { submitColor: () => null };

  let component: React.Component<Props, State>;
  let componentDOMNode: Element;
  let submitColorSpy: jest.SpyInstance;

  beforeEach(() => {
    submitColorSpy = jest.spyOn(eventHandlers, 'submitColor');
    component = TestUtils.renderIntoDocument(
      <ColorForm buttonText="Save Color"
        onSubmitColor={eventHandlers.submitColor} />
    ) as React.Component<Props, State>;
    componentDOMNode = TestUtils.findRenderedDOMComponentWithTag(component, 'form');
  });

  test('<ColorForm /> renders', () => {

    expect(component.props.onSubmitColor).toBe(submitColorSpy);
    expect(component.props.buttonText).toBe('Save Color');
    expect(component.state.color).toBe('');

    const inputDOMNode = componentDOMNode.querySelector('input') as HTMLInputElement;
    expect(inputDOMNode.value).toBe('');

    inputDOMNode.value = colorValue;
    TestUtils.Simulate.change(inputDOMNode);

    expect(component.state.color).toBe(colorValue);

    TestUtils.Simulate.click(componentDOMNode.querySelector('button') as HTMLButtonElement);

    expect(submitColorSpy).toHaveBeenCalledWith(colorValue);
  });

});

describe('<ColorForm /> Enzyme Mock DOM', () => {

  const colorValue = 'purple';
  const eventHandlers = { submitColor: () => null };

  let component: ReactWrapper;
  let submitColorSpy: jest.SpyInstance;

  beforeEach(() => {
    submitColorSpy = jest.spyOn(eventHandlers, 'submitColor');
    component = mount(<ColorForm buttonText="Save Color" onSubmitColor={eventHandlers.submitColor} />);
  });

  test('<ColorForm /> renders', () => {

    expect((component.props() as Props).onSubmitColor).toBe(submitColorSpy);
    expect((component.props() as Props).buttonText).toBe('Save Color');
    expect((component.state() as State).color).toBe('');

    const colorInput = component.find('input');
    expect(colorInput.prop('value')).toBe('');

    colorInput.simulate('change', {
      target: { value: colorValue, name: 'color' },
      currentTarget: { value: colorValue }, name: 'color',
    });

    expect((component.state() as State).color).toBe(colorValue);

    component.find('button').simulate('click');

    expect(submitColorSpy).toHaveBeenCalledWith(colorValue);
  });

});