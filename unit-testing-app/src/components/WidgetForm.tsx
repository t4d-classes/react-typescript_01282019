import * as React from 'react';

import { Widget } from '../models/Widget';

export interface Props {
  buttonText: string;
  onSubmitWidget: (widget: Widget) => void;
}

export interface State {
  name: string;
  description: string;
  size: string
  color: string;
  price: number;
  quantity: number;
  [ x: string ]: any;  
}

export class WidgetForm extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = this.getInitialState();
  }

  public getInitialState() {
    return {
      name: '',
      description: '',
      size: '',
      color: '',
      price: 0,
      quantity: 0,
    };
  }

  public change = (evt: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [ evt.target.name ]: evt.target.type === 'number'
        ? Number(evt.target.value)
        : evt.target.value
    });
  };

  public submitWidget = () => {
    this.props.onSubmitWidget({ ...this.state } as Widget);
    this.setState(this.getInitialState());
  };

  public render() {
    return <form>
      <div>
        <label htmlFor="name-input">Name:</label>
        <input type="text" id="name-input" name="name" value={this.state.name} onChange={this.change} />
      </div>
      <div>
        <label htmlFor="description-input">Description:</label>
        <input type="text" id="description-input" name="description" value={this.state.description} onChange={this.change} />
      </div>
      <div>
        <label htmlFor="size-input">Size:</label>
        <input type="text" id="size-input" name="size" value={this.state.size} onChange={this.change} />
      </div>
      <div>
        <label htmlFor="color-input">Color:</label>
        <input type="text" id="color-input" name="color" value={this.state.color} onChange={this.change} />
      </div>
      <div>
        <label htmlFor="price-input">Price:</label>
        <input type="number" id="price-input" name="price" value={this.state.price} onChange={this.change} />
      </div>
      <div>
        <label htmlFor="quantity-input">Quantity:</label>
        <input type="number" id="quantity-input" name="quantity" value={this.state.quantity} onChange={this.change} />
      </div>
      <button type="button" onClick={this.submitWidget}>{this.props.buttonText}</button>
    </form>;
  }

}