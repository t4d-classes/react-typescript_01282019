import * as React from 'react';
import { render, mount, shallow, ReactWrapper, ShallowWrapper, configure } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';

import { Widget } from '../models/Widget';
import { WidgetViewRow } from './WidgetViewRow';

configure({ adapter: new Adapter() });

describe('<WidgetViewRow /> Enzyme Static HTML', () => {

  let widget: Widget;

  beforeEach(() => {

    widget = {
      id: 1,
      name: 'Test Widget',
      description: 'Test Widget Description',
      color: 'red',
      size: 'small',
      price: 10.0,
      quantity: 20,
    };

  });

  test('<WidgetViewRow /> renders', () => {

    const component = JSON.stringify(render(
      <table>
        <tbody>
          <WidgetViewRow widget={widget} onDeleteWidget={() => null} onEditWidget={() => null} />
        </tbody>
      </table>
    ).html());

    expect(component).toMatchSnapshot();

  });

});

describe('<WidgetViewRow /> Enzyme Mock DOM', () => {

  const eventHandlers = {
    deleteWidget: () => null,
    editWidget: () => null,
  };

  let widget: Widget;
  let deleteWidgetSpy: jest.SpyInstance;
  let editWidgetSpy: jest.SpyInstance;
  let component: ReactWrapper;

  beforeEach(() => {

    widget = {
      id: 1,
      name: 'Test Widget',
      description: 'Test Widget Description',
      color: 'red',
      size: 'small',
      price: 10.0,
      quantity: 20,
    };

    deleteWidgetSpy = jest.spyOn(eventHandlers, 'deleteWidget');
    editWidgetSpy = jest.spyOn(eventHandlers, 'editWidget');

    component = mount(<table><tbody>
      <WidgetViewRow widget={widget}
        onDeleteWidget={eventHandlers.deleteWidget}
        onEditWidget={eventHandlers.editWidget} />  
    </tbody></table>);

  });

  test('<WidgetViewRow /> renders', () => {

    const columns = ['id', 'name', 'description', 'color', 'size', 'price', 'quantity'];

    component.find('td').slice(0,6).forEach( (node, index) => {
      const widgetField = String(widget[columns[index]]);
      expect(node.text()).toBe(widgetField);
    } );

  });

  test('<WidgetViewRow /> edit button', () => {

    component.find('button').first().simulate('click');
    expect(editWidgetSpy).toHaveBeenCalledWith(widget.id);

  });

  test('<WidgetViewRow /> delete button', () => {

    component.find('button').last().simulate('click');
    expect(deleteWidgetSpy).toHaveBeenCalledWith(widget.id);

  });

});

describe('<ToolHeader /> Shallow with Enzyme', () => {

  const eventHandlers = {
    deleteWidget: () => null,
    editWidget: () => null,
  };

  let widget: Widget;
  let deleteWidgetSpy: jest.SpyInstance;
  let editWidgetSpy: jest.SpyInstance;
  let wrapper: ShallowWrapper;

  beforeEach(() => {

    widget = {
      id: 1,
      name: 'Test Widget',
      description: 'Test Widget Description',
      color: 'red',
      size: 'small',
      price: 10.0,
      quantity: 20,
    };

    deleteWidgetSpy = jest.spyOn(eventHandlers, 'deleteWidget');
    editWidgetSpy = jest.spyOn(eventHandlers, 'editWidget');

    wrapper = shallow(
      <WidgetViewRow widget={widget}
        onDeleteWidget={eventHandlers.deleteWidget}
        onEditWidget={eventHandlers.editWidget} />  
    );

  });

  test('<WidgetViewRow /> renders', () => {

    const columns = ['id', 'name', 'description', 'color', 'size', 'price', 'quantity'];

    wrapper.find('td').slice(0,6).forEach( (node, index) => {
      const widgetField = String(widget[columns[index]]);
      expect(node.text()).toBe(widgetField);
    } );

  });

  test('<WidgetViewRow /> edit button', () => {

    wrapper.find('button').first().simulate('click');
    expect(editWidgetSpy).toHaveBeenCalledWith(widget.id);

  });

  test('<WidgetViewRow /> delete button', () => {

    wrapper.find('button').last().simulate('click');
    expect(deleteWidgetSpy).toHaveBeenCalledWith(widget.id);

  }); 

});
