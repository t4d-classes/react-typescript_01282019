import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

// export interface Props extends RouteComponentProps {

// }

export type Props = RouteComponentProps;

export const Home = ({ location }: Props) => {

  console.log(location);

  return <div>
    Home
  </div>;

};