import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { carToolStore } from './carToolStore';
import { CarToolContainer } from './containers/CarToolContainer';

import './index.css';

ReactDOM.render(
  <Provider store={carToolStore}>
    <CarToolContainer />
  </Provider>,
  document.querySelector('#root') as HTMLElement
);

