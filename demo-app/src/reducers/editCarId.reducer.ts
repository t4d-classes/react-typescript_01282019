import { Action } from 'redux';

import { CarActions, CarEditAction } from '../actions/car.actions';

export const editCarIdReducer = (state: number = -1, action: Action<string>) => {

  if ([
      CarActions.APPEND as string,
      CarActions.REPLACE as string,
      CarActions.DELETE as string,
      CarActions.CANCEL as string,
    ].includes(action.type)) {
    return -1;
  }

  if (action.type === CarActions.EDIT) {
    return (action as CarEditAction).payload;
  }

  return state;

}