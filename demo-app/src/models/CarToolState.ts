import { Car } from './Car';

export interface CarToolState {
  cars: Car[];
  editCarId: number;
}