import React, { useState, useEffect, useRef, forwardRef, Ref } from 'react';

interface ColorFormProps {
  onSubmitColor: (color: string) => void;
  buttonText?: string;
}

export const ColorForm =  forwardRef( (
  { onSubmitColor, buttonText }: ColorFormProps,
  colorInputRef: Ref<HTMLInputElement>) => {

  const [ color, setColor ] = useState('');

  const colorInput: any = colorInputRef || useRef<HTMLInputElement>(null); // React.createRef<HTMLInputElement>();

  if (!colorInputRef) {
    useEffect(() => {
      if (colorInput.current) {
        colorInput.current.focus();
      }
    });
  }

  const submitColor = () => {
    onSubmitColor(color);
    setColor('');
  };

  return <form>
    <div>
      <label htmlFor="color-input">New Color:</label>
      <input type="text" id="color-input" name="color" ref={colorInput}
        value={color} onChange={e => setColor(e.target.value)} />
    </div>
    <button type="button" onClick={submitColor}>{buttonText}</button>
  </form>;
} );