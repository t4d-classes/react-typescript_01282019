import React from 'react';

export const HelloWorld = () => {
  return <header>
    <h1>Hello World!</h1>
    <h2>Subtitle</h2>
  </header>;
  // return React.createElement('header', null,
  //          React.createElement('h1', null, 'Hello World!'),
  //          React.createElement('h2', null, 'Subtitle')
  // );
};