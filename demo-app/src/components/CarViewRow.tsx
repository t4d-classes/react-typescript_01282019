import React from 'react';

import { Car } from '../models/Car';

interface CarViewRowProps {
  car: Car;
  onEditCar: (carId: number) => void;
  onDeleteCar: (carId: number) => void;
}

export const CarViewRow = ({ car, onEditCar, onDeleteCar }: CarViewRowProps) => {

  const editCar = () => onEditCar(car.id);
  const deleteCar = () => onDeleteCar(car.id);

  return <tr>
    <td>{car.id}</td>
    <td>{car.make}</td>
    <td>{car.model}</td>
    <td>{car.year}</td>
    <td>{car.color}</td>
    <td>{car.price}</td>
    <td>
      <button type="button" onClick={editCar}>Edit</button>
      <button type="button" onClick={deleteCar}>Delete</button>
    </td>
  </tr>;
};