import React, { useState, useEffect, useRef } from 'react';

import { Car } from '../models/Car';

interface CarEditRowProps {
  car: Car;
  onSaveCar: (car: Car) => void;
  onCancelCar: () => void;
}

const setValue: { [x: string]: any } = {
  number: (v: string) => Number(v),
  text: (v: string) => v,
};

const setObject = (setForm: Function, form: Object) => {
  return ({ target: { type, name, value } }: React.ChangeEvent<HTMLInputElement>) => {
    setForm({
      ...form,
      [ name ]: setValue[type](value),
    })
  };
};

export const CarEditRow = ({ car, onSaveCar, onCancelCar }: CarEditRowProps) => {

  const [ form, setForm ] = useState({
    make: car.make,
    model: car.model,
    year: car.year,
    color: car.color,
    price: car.price,
  });

  const modelInputRef = useRef<HTMLInputElement>(null);

  useEffect(() => { modelInputRef.current && modelInputRef.current.focus() });

  return <tr>
    <td>{car.id}</td>
    <td><input type="text" name="make" value={form.make} onChange={setObject(setForm, form)} /></td>
    <td><input type="text" name="model" value={form.model} onChange={setObject(setForm, form)} ref={modelInputRef} /></td>
    <td><input type="number" name="year" value={form.year} onChange={setObject(setForm, form)} /></td>
    <td><input type="text" name="color" value={form.color} onChange={setObject(setForm, form)} /></td>
    <td><input type="number" name="price" value={form.price} onChange={setObject(setForm, form)} /></td>
    <td>
      <button type="button" onClick={() => onSaveCar({
        ...form, id: car.id })}>Save</button>
      <button type="button" onClick={onCancelCar}>Cancel</button>
    </td>
  </tr>;
};