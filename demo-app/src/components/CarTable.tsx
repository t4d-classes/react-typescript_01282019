import React from 'react';

import { Car } from '../models/Car';

import { CarViewRow } from './CarViewRow';
import { CarEditRow } from './CarEditRow';

interface CarTableProps {
  cars: Car[],
  editCarId: number,
  onEditCar: (carId: number) => void;
  onDeleteCar: (carId: number) => void;
  onSaveCar: (car: Car) => void;
  onCancelCar: () => void;
}

export const CarTable  = React.memo(({
  cars, editCarId, onEditCar,
  onDeleteCar, onSaveCar, onCancelCar,
}: CarTableProps) =>
  <table>
    <thead>
      <tr>
        <th>Id</th>
        <th>Make</th>
        <th>Model</th>
        <th>Year</th>
        <th>Color</th>
        <th>Price</th>
      </tr>
    </thead>
    <tbody>
      {cars.map(car => car.id === editCarId
        ? <CarEditRow key={car.id} car={car} onSaveCar={onSaveCar} onCancelCar={onCancelCar} />
        : <CarViewRow key={car.id} car={car} onEditCar={onEditCar} onDeleteCar={onDeleteCar} />)}
    </tbody>
</table>);