import React, { useRef, useEffect } from 'react';

import { Car } from '../models/Car';

import { ToolHeader } from './ToolHeader';
import { CarTable } from './CarTable';
import { CarFormWrapper } from './CarForm';

interface CarToolProps {
  cars: Car[];
  editCarId: number;
  onRefresh: () => void;
  onAppend: Function;
  onReplace: Function;
  onEdit: (editCarId: number) => void;
  onDelete: Function;
  onCancel: Function;
}

export const CarTool = (props: CarToolProps) => {

  const {
    cars, editCarId, onRefresh, onAppend,
    onEdit, onReplace, onDelete, onCancel,
  } = props;

  const defaultInputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    onRefresh();

    // return () => {
    //   // gets called when the component is removed/unmounted
    //   console.log('test');  
    // };
  }, []);

  const editCar = (editCarId: number) => {
    onEdit(editCarId);
  }

  const cancelCar = () => {
    // noop
    onCancel();
    defaultInputRef.current && defaultInputRef.current.focus();
  };

  const addCar = (car: Car) => {
    onAppend(car);
  };

  const replaceCar = (car: Car) => {
    onReplace(car);
    defaultInputRef.current && defaultInputRef.current.focus();
  };

  const deleteCar = (carId: number) => {
    // noop
    onDelete(carId);
  };

  return <>
    <ToolHeader headerText="Car Tool" />
    <CarTable cars={cars} editCarId={editCarId}
      onEditCar={editCar} onDeleteCar={deleteCar}
      onSaveCar={replaceCar} onCancelCar={cancelCar} />
    <CarFormWrapper onSubmitCar={addCar} buttonText="Add Car" ref={defaultInputRef} />
  </>;

};