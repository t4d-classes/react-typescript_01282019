import React from 'react';

import { Car } from '../models/Car';
import { threadId } from 'worker_threads';

interface CarFormProps {
  onSubmitCar: (car: Car) => void;
  buttonText?: string;
  innerRef: React.Ref<HTMLInputElement>;
}

interface CarFormState {
  make: string;
  model: string;
  year: number;
  color: string;
  price: number;
  [ x: string ]: any;
}

export class CarForm extends React.Component<CarFormProps, CarFormState> {

  state = {
    make: '',
    model: '',
    year: 1900,
    color: '',
    price: 0,
  };

  static defaultProps = {
    buttonText: 'Submit Car',
  };

  change = ({ target: { type, name, value } }: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [ name ]: type === 'number' ? Number(value) : value,
    });
  };

  submitCar = () => {
    this.props.onSubmitCar({ ...this.state } as Car);
    this.setState({
      make: '',
      model: '',
      year: 1900,
      color: '',
      price: 0,
    })
  }

  render() {
    return <form>
      <div>
        <label htmlFor="make-input">Make:</label>
        <input type="text" id="make-input" name="make"
          value={this.state.make} onChange={this.change} ref={this.props.innerRef} />
      </div>
      <div>
        <label htmlFor="model-input">Model:</label>
        <input type="text" id="model-input" name="model"  
          value={this.state.model} onChange={this.change} />
      </div>
      <div>
        <label htmlFor="year-input">Year:</label>
        <input type="number" id="year-input" name="year"
          value={this.state.year} onChange={this.change} />
      </div>
      <div>
        <label htmlFor="color-input">Color:</label>
        <input type="text" id="color-input" name="color"
          value={this.state.color} onChange={this.change} />
      </div>
      <div>
        <label htmlFor="price-input">Price:</label>
        <input type="number" id="price-input" name="price"
          value={this.state.price} onChange={this.change} />
      </div>
      <button type="button" onClick={this.submitCar}>{this.props.buttonText}</button>
    </form>;
  }  

}

export const CarFormWrapper = React.forwardRef(
  (props: any, ref: React.Ref<HTMLInputElement>) => <CarForm {...props} innerRef={ref} />);