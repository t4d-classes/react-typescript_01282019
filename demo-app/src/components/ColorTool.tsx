import React from 'react';

import { ToolHeader } from './ToolHeader';
import { ColorForm } from './ColorForm';

interface ColorToolProps {
  colors: string[];
}

interface ColorToolState {
  colors: string[];
}

export class ColorTool extends React.Component<ColorToolProps,ColorToolState> {

  colorInput = React.createRef<HTMLInputElement>();

  state = {
    colors: this.props.colors.slice(),
  };

  componentDidMount() {
    if (this.colorInput.current) {
      this.colorInput.current.focus();
    }
  }

  addColor = (color: string) => {
    this.setState({
      colors: this.state.colors.concat(color),
    });
  };

  render() {
    return <>
      <ToolHeader headerText="Color Tool" />
      <ul>
        {this.state.colors.map( (color) => <li key={color}>{color}</li> )}
      </ul>
      <ColorForm buttonText="Add Color" onSubmitColor={this.addColor} ref={this.colorInput} />
    </>;
  }

};