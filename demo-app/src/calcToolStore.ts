import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { actionFromClassMiddleware } from "./middleware/actionFromClass";

import { calcReducer } from './reducers/calcReducer';

export const calcToolStore = createStore(
  calcReducer,
  composeWithDevTools(applyMiddleware(actionFromClassMiddleware)),
);