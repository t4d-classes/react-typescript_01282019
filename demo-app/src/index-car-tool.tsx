// import React from 'react';
// import ReactDOM from 'react-dom';

// import { ColorTool } from './components/ColorTool';
// import { CarTool } from './components/CarTool';

// import './index.css';

// const colorList = [ 'blue', 'green', 'orange', 'purple' ];
// // const carList = [
//   { id: 1, make: 'Ford', model: 'Fusion Hybrid', year: 2016, color: 'green', price: 25000 },
//   { id: 2, make: 'Tesla', model: 'Model S', year: 2017, color: 'red', price: 100000 },
// ];

// ReactDOM.render(
//   <>
//     <ColorTool colors={colorList} />
//     <CarTool cars={carList} />
//   </>,
//   // React.createElement(HelloWorld),
//   // React.createElement(ColorTool, { colors: colorList });
//   document.querySelector('#root')
// );

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { carToolStore } from './carToolStore';
import { CarToolContainer } from './containers/CarToolContainer';

import './index.css';

ReactDOM.render(
  <Provider store={carToolStore}>
    <CarToolContainer />
  </Provider>,
  document.querySelector('#root') as HTMLElement
);

